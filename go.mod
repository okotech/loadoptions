module gitlab.com/okotech/loadoptions

go 1.17

replace gitlab.com/okotek/okotypes => ../okotypes

require gitlab.com/okotek/okotypes v0.0.0-20220328184655-40ed17237fbd

require gocv.io/x/gocv v0.29.0 // indirect
