package loadoptions

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	types "gitlab.com/okotek/okotypes"
)

//Load returns a ConfigOptions structure
func Load() types.ConfigOptions {

	var characters []byte
	var configDir string = "/etc/okoConfig.conf"
	var retDat types.ConfigOptions
	var iIter int
	var err error
	var etcFileError error
	var fileError error
	characters, etcFileError = ioutil.ReadFile(configDir)

	if etcFileError != nil {
		//fmt.Println("Could not read config file.\nRetrying with local config.")
		characters, fileError = ioutil.ReadFile("../okoConfig.conf")
		if fileError != nil {
			log.Println("Error with reading config file. Going off of built-in configuration.")
		}
	}

	retDat.EyeballHighIndex = 1
	retDat.EyeballLowIndex = 0

	optionArray := strings.Split(string(characters), "\n")
	for _, iter := range optionArray {
		tmpSplit := strings.Split(iter, "=")
		//fmt.Println(iter)

		if len(tmpSplit) != 2 {
			continue
		}

		if tmpSplit[0] == "" || tmpSplit[0] == " " {
			continue
		}

		if strings.Contains(tmpSplit[0], "EyeballSenderAddr") {
			retDat.EyeballSenderAddr = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "EyeballLowIndex") {
			retDat.EyeballLowIndex, err = strconv.Atoi(tmpSplit[1])
		} else if strings.Contains(tmpSplit[0], "EyeballHighIndex") {
			retDat.EyeballHighIndex, err = strconv.Atoi(tmpSplit[1])
		} else if strings.Contains(tmpSplit[0], "EyeballPixelThreshold") {
			retDat.EyeballPixelThreshold, _ = strconv.Atoi(tmpSplit[1])
		} else if strings.Contains(tmpSplit[0], "EyeballPercentThreshold") {
			retDat.EyeballPercentThreshold, _ = strconv.Atoi(tmpSplit[1])
		} else if strings.Contains(tmpSplit[0], "EyeballSendoffPreProcessors") {
			retDat.EyeballSendoffPreProcessors, _ = strconv.Atoi(tmpSplit[1])
		} else if strings.Contains(tmpSplit[0], "EyeballUserName") {
			retDat.EyeballUserName = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "EyeballPassWord") {
			retDat.EyeballPassWord = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "EyeballUnitID") {
			retDat.EyeballUnitID = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "ClassifierListenPort") {
			retDat.ClassifierListenPort = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "ClassifierSendAddr") {
			retDat.ClassifierSendAddr = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "ClassifierXMLLocation") {
			retDat.ClassifierXMLLocation = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "StampMachineColorRGBA") {
			tmp := strings.Split(tmpSplit[1], ",")
			for count, iter := range tmp {
				iIter, err = strconv.Atoi(iter)
				retDat.StampMachineColorRGBA[count] = iIter
			}
		} else if strings.Contains(tmpSplit[0], "StampMachineListenerAddr") {
			retDat.StampMachineListenerAddr = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "StampMachineSendoffPort") {
			retDat.StampMachineSendoffPort = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "BackStackTattleSendoffAddr") {
			retDat.BackStackTattleSendoffAddr = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "BackStackStorageDirectory") {
			retDat.BackStackStorageDirectory = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "ServerStorageDirectory") {
			retDat.ServerStorageDirectory = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "SinceRewriteTime") {
			retDat.SinceRewriteDelay, err = strconv.ParseFloat(tmpSplit[1], 64)
		} else if strings.Contains(tmpSplit[0], "CheckFrameUpdateInterval") {
			retDat.CheckFrameUpdateInterval, err = strconv.ParseInt(tmpSplit[1], 10, 64)
		} else if strings.Contains(tmpSplit[0], "LogSendAddress") {
			retDat.LogSendAddress = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "LogListenPort") {
			retDat.LogListenPort = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "StringLogAddress") {
			retDat.StringLogAddress = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "StringListenAddress") {
			retDat.StringListenAddress = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "CounterListenAddress") {
			retDat.CounterListenAddress = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "CounterListenPort") {
			retDat.CounterListenPort = tmpSplit[1]
		} else if strings.Contains(tmpSplit[0], "LoggerFailKillProcess") {
			if strings.Contains(tmpSplit[1], "true") {
				retDat.LoggerFailKillProcess = true
			} else {
				retDat.LoggerFailKillProcess = false
			}
		} else if strings.Contains(tmpSplit[0], "DebugMode") {
			if tmpSplit[1] == "True" || tmpSplit[1] == "true" || tmpSplit[1] == "t" || tmpSplit[1] == "T" {
				retDat.DebugMode = true
			} else if tmpSplit[1] == "False" || tmpSplit[1] == "false" || tmpSplit[1] == "f" || tmpSplit[1] == "F" {
				retDat.DebugMode = false
			} else {
				fmt.Println("Got bad option for DebugMode in config file: ", tmpSplit[1])
			}

		} else {
			fmt.Println("Config variable not matched. Iter:", iter, "\r")
		}

		if err != nil {
			fmt.Println("Error in loadOptions function:", err)
		}

	}

	fmt.Println("Loaded data in loadOptions:", retDat)
	//time.Sleep(10 * time.Second)
	retDat.StringListenAddress = "localhost:9090"

	return retDat
}
